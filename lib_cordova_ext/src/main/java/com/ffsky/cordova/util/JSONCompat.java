package com.ffsky.cordova.util;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

public class JSONCompat {

    public static int readInt(String key, JSONObject json) {
        if (json == null) {
            return -1;
        }
        if (json.containsKey(key)) {
            return json.getIntValue(key);
        }
        return -1;
    }

    public static Boolean readBoolean(String key, JSONObject json) {
        if (json == null) {
            return null;
        }
        try {
            if (json.containsKey(key)) {
                return json.getBoolean(key);
            }
        } catch (Exception ex) {
        }
        return null;
    }
    public static boolean readBoolean(String key, JSONObject json,boolean defaultValue) {
        if (json == null) {
            return defaultValue;
        }
        try {
            if (json.containsKey(key)) {
                return json.getBoolean(key);
            }
        } catch (Exception ex) {
        }
        return defaultValue;
    }

    public static String readString(String key, JSONObject json) {
        if (json == null) {
            return null;
        }
        if (json.containsKey(key)) {
            return json.getString(key);
        }

        return null;
    }

    public static JSONObject readObject(String key, JSONObject json) {
        if (json == null) {
            return null;
        }
        try {
            if (json.containsKey(key)) {
                return json.getJSONObject(key);
            }
        } catch (Exception ex) {
        }
        return null;
    }
    public static JSONArray readArray(String key, JSONObject json) {
        if (json == null) {
            return null;
        }
        try {
            if (json.containsKey(key)) {
                return json.getJSONArray(key);
            }
        } catch (Exception ex) {
        }
        return null;
    }
}
