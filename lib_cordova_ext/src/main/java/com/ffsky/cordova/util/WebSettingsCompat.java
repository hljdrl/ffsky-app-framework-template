package com.ffsky.cordova.util;

import android.os.Build;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.WebSettings;

import com.ffsky.litepack.app.AppConf;
import com.ffsky.logger.L;

import org.apache.cordova.engine.SystemWebView;

public class WebSettingsCompat {

    public static final String USER_AGENT = "AppUserAgent/";

    public static void settings(SystemWebView webView) {
        if(webView==null){
            return;
        }
        WebSettings settings = webView.getSettings();
        settings.setAllowUniversalAccessFromFileURLs(true);
        settings.setAllowFileAccess(true);
        settings.setAllowFileAccessFromFileURLs(true);
        //-----------------------------------------------
        webView.setFadingEdgeLength(0);
        webView.setHorizontalFadingEdgeEnabled(false);
        webView.setVerticalFadingEdgeEnabled(false);
        //
        webView.setHorizontalScrollBarEnabled(false);//水平不显示
        webView.setVerticalScrollBarEnabled(false); //垂直不显示
        //
        //
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            settings.setMediaPlaybackRequiresUserGesture(false);
        }
//        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        settings.setLoadWithOverviewMode(true);
        //
        webView.setOverScrollMode(View.OVER_SCROLL_NEVER);
        settings.setDomStorageEnabled(true);    //开启DOM形式存储
        settings.setDatabaseEnabled(true);   //开启数据库形式存储
        //
        settings.setAppCacheEnabled(true);
        settings.setSupportZoom(false);
        settings.setDisplayZoomControls(false);
        settings.setBuiltInZoomControls(false);

//        settings.setRenderPriority(WebSettings.RenderPriority.HIGH);
        ////将图片调整到适合webview的大小
        settings.setUseWideViewPort(true);
        settings.setLoadsImagesAutomatically(true); //支持自动加载图片
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            settings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
            CookieManager.getInstance().setAcceptThirdPartyCookies(webView, true);
        }
        //-----------------------------------------------
        String ua = settings.getUserAgentString();
        // 设置浏览器UA,JS端通过UA判断是否属于Quick环境
        settings.setUserAgentString(WebSettingsCompat.setUserAgent(ua));

    }

    public  static String setUserAgent(String _ua) {
        StringBuffer buf = new StringBuffer();
        buf.append(_ua);
        buf.append(" ");
        buf.append(USER_AGENT);
        buf.append(AppConf.vName);
        buf.append("/");
        L.i("WebSettingsCompat", "setUserAgent-->", buf.toString());
        return buf.toString();
    }

    public static void clearUserCookie() {
        try {
            CookieManager cookieManager = CookieManager.getInstance();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                cookieManager.removeSessionCookies(null);
                cookieManager.removeAllCookies(null);
                cookieManager.flush();
            }
//            else {
//                cookieManager.removeAllCookie();
//            }
//            WebStorage.getInstance().deleteAllData();
        }catch(NoSuchMethodError | Exception error){
            error.printStackTrace();
        }
    }
}
