package com.ffsky.cordova.util;

import android.graphics.Color;
import android.text.TextUtils;

public class ColorCompat {


    public static int parseColor(String color, int defaultValue) {
        int ret = defaultValue;
        try {
            if (!TextUtils.isEmpty(color)) {
                ret = Color.parseColor(color);
            }
        } catch (Exception ex) {

        }
        return ret;
    }
}
