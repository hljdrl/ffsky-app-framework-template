package com.ffsky.cordova.option;

import android.os.Parcel;

public class WebOption implements android.os.Parcelable {
    /**
     * url网址
     */
    public String url;
    /**
     * 1=原路返回,2=返回页面第一个url地址,3=WebView页面销毁
     * webview-退栈模式
     */
    public int backMode = 1;
    /**
     * 显示Toolbar标题栏
     */
    public boolean showToolBar = true;
    /**
     * 显示系统状态栏
     */
    public boolean showStatusBar = true;
    /**
     * Toolbar背景颜色
     */
    public int toolbarColor = 0;
    /**
     * 系统状态栏颜色
     */
    public int statusBarColor = 0;

    public String title;

    public boolean updateTitle = true;

    public WebOption(String url){
        this.url = url;
    }

    protected WebOption(Parcel in) {
        url = in.readString();
        backMode = in.readInt();
        showToolBar = in.readByte() != 0;
        showStatusBar = in.readByte() != 0;
        toolbarColor = in.readInt();
        statusBarColor = in.readInt();
        title = in.readString();
        updateTitle = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(url);
        dest.writeInt(backMode);
        dest.writeByte((byte) (showToolBar ? 1 : 0));
        dest.writeByte((byte) (showStatusBar ? 1 : 0));
        dest.writeInt(toolbarColor);
        dest.writeInt(statusBarColor);
        dest.writeString(title);
        dest.writeByte((byte) (updateTitle ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<WebOption> CREATOR = new Creator<WebOption>() {
        @Override
        public WebOption createFromParcel(Parcel in) {
            return new WebOption(in);
        }

        @Override
        public WebOption[] newArray(int size) {
            return new WebOption[size];
        }
    };
}
