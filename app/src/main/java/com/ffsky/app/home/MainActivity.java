package com.ffsky.app.home;

import android.Manifest;
import android.os.Bundle;
import android.view.View;

import com.ffsky.app.databinding.ActivityMainBinding;
import com.ffsky.cordova.CordovaKit;
import com.ffsky.cordova.app.AppWebActivity;
import com.ffsky.cordova.app.WebActivity;
import com.ffsky.cordova.plugin.PageJsPlugin;
import com.ffsky.jsplugin.JsPluginManager;
import com.ffsky.litepack.Lite;
import com.ffsky.litepack.util.MD5Compat;
import com.ffsky.logger.L;
import com.ffsky.smtt.X5ModularLoader;
import com.ffsky.smtt.X5WebActivity;

import org.json.JSONException;
import org.json.JSONObject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

public class MainActivity extends AppCompatActivity {

    final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        L.i(TAG, "onCreate");
        ActivityMainBinding binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        //
        binding.btnCordovaActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppWebActivity.start(MainActivity.this, null);
            }
        });

        binding.btnCordovaFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CordovaFragmentActivity.start(MainActivity.this);
            }
        });
        binding.btnAppWebActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppWebActivity.start(MainActivity.this, null);
            }
        });

        binding.btnWebActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JsPluginManager.getInstance().register("page", PageJsPlugin.class);

                WebActivity.start(MainActivity.this, "http://www.baidu.com"
                        , WebActivity.BACK_MODE_DEFAULT, null);
            }
        });

        binding.btnUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                WebActivity.start(MainActivity.this, "https://gitee.com/hljdrl/cordova-publish");
                AppWebActivity.start(MainActivity.this, "http://sznhl.rarb.cn/port/html/block/test?token=eyJhbGciOiJIUzI1NiJ9eyJzdWIiOiJhZG1pbiIsImlhdCI6MTYyNTQ3NTM2MCwidXNlcklkIjoxLCJyZWFsTmFtZSI6Iui2hee6p-euoeeQhuWRmCIsImFnZW50Q29kZSI6ImFwcCIsImNyZWF0ZVRpbWUiOiIyMDIxMDcwNTE2NTYwMCIsImVudk5hbWUiOiJzaXQiLCJvcmdDb2RlIjoiMzMwMzgxIiwiYXBwQ29kZSI6InN0cmluZyIsInJvbGVDb2RlIjoiZmFybWVyLGZhcm1fd29ya2VyIiwiZXhwIjoxNjI2MDgwMTYwfQ.FhKP3GsdBiN9TVO3kL-RSWldl9mdtKrnoGY0M6-fQF8");
            }
        });


        binding.btnHomeTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeTabActivity.start(MainActivity.this);
            }
        });

        binding.btnX5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                X5WebActivity.start(MainActivity.this, "http://soft.imtt.qq.com/browser/tes/feedback.html");
                X5WebActivity.start(MainActivity.this, "http://debugtbs.qq.com");
            }
        });
        binding.btnX5webview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                X5WebActivity.start(MainActivity.this, null);
            }
        });

        binding.btnLoggerOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                L.open();
                L.i(TAG, "open logger");
            }
        });
        binding.btnLoggerClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                L.close();
                L.i(TAG, "close logger");
            }
        });


        CordovaKit.getInstance().load(getApplicationContext());

        /**
         * 实际代码接入，最好放到 Application的onCreate方法中初始化，提前加载下载X5内核
         *
         * */
        X5ModularLoader.getInstance().load(getApplication(), null);

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE);

        Lite.kvCache.save(MD5Compat.getStringMD5("__update_time"), String.valueOf(System.currentTimeMillis()));

        String cache = Lite.kvCache.read(MD5Compat.getStringMD5("__update_time"));
        L.i(TAG, "db read=", cache);
    }

    final int REQUEST_CODE = 301;

    @Override
    protected void onResume() {
        super.onResume();
        L.i(TAG, "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        JSONObject json = new JSONObject();
        try {
            json.putOpt("a", 1);
            json.putOpt("b", 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        L.i(TAG, "onPause ", json);
    }
}