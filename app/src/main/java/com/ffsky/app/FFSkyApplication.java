package com.ffsky.app;

import android.app.Application;

import com.ffsky.app.builder.ActivityLifecycleListener;
import com.ffsky.app.builder.AppConfImpl;
import com.ffsky.litepack.Lite;
import com.ffsky.litepack.app.AppConf;
import com.ffsky.litepack.util.ClassCompat;
import com.ffsky.logger.L;

public class FFSkyApplication extends Application {

    private ActivityLifecycleListener mActivityLifecycleListener;
    @Override
    public void onCreate() {
        super.onCreate();
//        viseLogInit();
        mActivityLifecycleListener = new ActivityLifecycleListener();
        AppConf appConf = new AppConfImpl(BuildConfig.DEBUG,BuildConfig.APPLICATION_ID,
                BuildConfig.VERSION_CODE,BuildConfig.VERSION_NAME, ClassCompat.forValue(BuildConfig.class),"dev");
        new Lite.Builder()
                .setAppInterface(mActivityLifecycleListener)
                .setApplication(this)
                .setAppConf(appConf)
                .apply();
        registerActivityLifecycleCallbacks(mActivityLifecycleListener);
        L.open();
        //-----------------------------------------------------------------
        L.i("FFSkyApplication", "onCreate");

    }
//    private void viseLogInit(){
//        File file = getCacheDir();
//        L.setLogOut(new ViseLogOut(getApplicationContext(),file.toString(),"skyLOG"));
//    }
//    private void tinyLogInit(){
        //tinylog初始化
//        File file = new File(getCacheDir(), "app.log");
//        String logFile = file.toString();
//        L.setLogOut(new TinyLogFileLogout(StorageUtil.alternative(logFile, StorageType.TYPE_LOG)));
        //-----------------------------------------------------------------
//    }
//    private void xLogInit(){
//        //xlog初始化
//        //-----------------------------------------------------------------
//        File file = new File(getCacheDir()+File.separator+"marslog");
//        //日志写文件并且logcat输出
//        //lib_X log日志
//        String cacheFile =getFilesDir()+"/xlogcache";
//        L.setLogOut(new MarsXLogout(file.toString(),cacheFile,"appxlog",true));
//        //-----------------------------------------------------------------
//    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        L.i("FFSkyApplication", "onTerminate");
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        L.i("FFSkyApplication", "onLowMemory");
    }
}
