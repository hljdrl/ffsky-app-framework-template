package com.ffsky.app.builder;


import com.ffsky.litepack.app.AppConf;

import java.util.HashMap;
import java.util.Map;

public class AppConfImpl extends AppConf {


    private Map<String, Object> readOnlyMap;

    public AppConfImpl(boolean debug, String pkg, int vcode, String vName, Map<String, Object> map, String env) {
        super(debug, pkg, vcode,vName, env);
        readOnlyMap = new HashMap(16);
        if (map != null && !map.isEmpty()) {
            readOnlyMap.putAll(map);
        }
    }

    @Override
    public String conf(String key) {
        Object value = readOnlyMap.get(key);
        if (value != null) {
            return value.toString();
        }
        return null;
    }
}
