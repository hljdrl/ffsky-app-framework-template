package com.ffsky.app.builder;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import com.ffsky.litepack.app.AppInterface;
import com.ffsky.litepack.util.ActivityUtil;
import com.ffsky.logger.L;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class ActivityLifecycleListener implements Application.ActivityLifecycleCallbacks, AppInterface {

    private final String TAG = "Lifecycle";
    private boolean appGoBackGround = false;
    private final AtomicInteger atomicInteger = new AtomicInteger(0);
    private final List<Activity> activityList = new ArrayList<>();

    private Activity resumedActivity;

    public ActivityLifecycleListener() {
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        if (activity != null) {
            activityList.add(activity);
            String clsName = activity.getClass().getName();
            L.i(TAG, "Created Activity->", clsName, " ,backgrounder=", Boolean.toString(appGoBackGround));
//            if(TextUtils.equals(clsName,"com.mobile.auth.gatewayauth.LoginAuthActivity")){
//                StatusBarUtil.setColor(activity, Color.BLACK);
//            }
        }
    }

    @Override
    public void onActivityStarted(Activity activity) {
        atomicInteger.incrementAndGet();
        int refCount = atomicInteger.get();
        if (refCount > 0) {
            setAppGoBackGround(false);
        }
        L.i(TAG, "Started->Count:", String.valueOf(refCount),
                ", backgrounder=", Boolean.toString(appGoBackGround));
    }

    @Override
    public void onActivityResumed(Activity activity) {
        String clsName = activity.getClass().getSimpleName();
        L.i(TAG, "ActivityResumed->", clsName);
        resumedActivity = activity;
    }

    @Override
    public void onActivityPaused(Activity activity) {
        String name = activity.getClass().getSimpleName();
        L.i(TAG, "ActivityPaused->", name);
    }

    @Override
    public void onActivityStopped(Activity activity) {
        atomicInteger.decrementAndGet();
        int refCount = atomicInteger.get();
        if (refCount == 0) {
            setAppGoBackGround(true);
            resumedActivity = null;
        } else if (refCount > 0) {
            setAppGoBackGround(false);
        }
        String name = activity.getClass().getSimpleName();
        L.i(TAG, "ActivityStopped->", name, " Stopped->Count:", String.valueOf(refCount),
                ", backgrounder=", Boolean.toString(appGoBackGround));
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        activityList.remove(activity);
        int size = activityList.size();
        String name = activity.getClass().getSimpleName();
        L.i(TAG, "ActivityDestroyed->", name, " activityCount=", size);
    }

    public boolean isAppGoBackGround() {
        return appGoBackGround;
    }

    public void setAppGoBackGround(boolean appGoBackGround) {
        this.appGoBackGround = appGoBackGround;
    }

    //======================================================================
    @Override
    public void closeApp() {
        closeAllActivity();
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    @Override
    public void closeAllActivity() {
        L.i(TAG, "closeAllActivity");
        List<Activity> copy = new ArrayList<>(activityList);
        activityList.clear();
        for (Activity activity : copy) {
            if (!ActivityUtil.isDestroyed(activity)) {
                activity.finish();
            }
        }
    }

    @Override
    public WeakReference<Activity> getCurrentActivity() {
        return new WeakReference<>(resumedActivity);
    }

    @Override
    public void goAppHome(boolean closeSecondAllActivity) {
//        boolean hasHome = false;
//        if (closeSecondAllActivity) {
//            List<Activity> copy = new ArrayList<>(activityList);
//            for (Activity aty : copy) {
//                if (!ActivityUtil.isDestroyed(aty)) {
//                    if (aty instanceof HomeActivity) {
//                        hasHome = true;
//                    } else {
//                        aty.finish();
//                    }
//                }
//            }
//        }
//        if (resumedActivity != null && !hasHome) {
//            Intent i = new Intent(resumedActivity, HomeActivity.class);
//            resumedActivity.startActivity(i);
//        }
    }

    @Override
    public void goLogin(Activity activity) {
    }

    @Override
    public void goAbout(Activity activity) {
    }

    @Override
    public void agreePrivacyAgreement() {
    }
}
