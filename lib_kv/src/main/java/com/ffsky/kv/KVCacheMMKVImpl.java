package com.ffsky.kv;

import android.content.Context;

import com.ffsky.litepack.cache.KVCache;
import com.ffsky.logger.L;
import com.tencent.mmkv.MMKV;

public class KVCacheMMKVImpl implements KVCache {
    final String TAG="KVImpl";
    private MMKV kv;

    public KVCacheMMKVImpl(Context context)
    {

        String rootDir = MMKV.initialize(context);
        L.i(TAG,"mmkv root: ",rootDir);
        kv = MMKV.defaultMMKV();
    }
    @Override
    public void save(String k, String v) {
        kv.encode(k,v);
    }

    @Override
    public String read(String k) {
        return kv.decodeString(k);
    }

    @Override
    public void del(String k) {
        kv.remove(k);
    }

    @Override
    public String read(String k, String value) {
        return kv.decodeString(k,value);
    }

    @Override
    public int readToInt(String k, int value) {
        return kv.decodeInt(k,value);
    }

    @Override
    public void clear() {
        kv.clearAll();
    }
}
