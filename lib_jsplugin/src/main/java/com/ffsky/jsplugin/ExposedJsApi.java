package com.ffsky.jsplugin;


import com.ffsky.litepack.disposable.Disposable;

public interface ExposedJsApi extends Disposable {
    /**
     * 桥接WebView调用js
     *
     * @param js
     */
    void evaluateJavascript(String js);

    /**
     * 获取WebView.getUrl();
     *
     * @return
     */
    String getWebViewUrl();
}
