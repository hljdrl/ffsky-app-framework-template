package com.ffsky.jsplugin;

import android.app.Activity;
import android.content.Intent;

import com.alibaba.fastjson.JSONObject;
import com.ffsky.litepack.disposable.Disposable;
import com.ffsky.logger.L;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import androidx.annotation.Nullable;

/**
 * JsPlugin插件
 */
public abstract class JsPlugin {

    private Activity mActivity;
    private ExposedJsApi mExposedJsApi;
    final protected String TAG = getClass().getSimpleName();
    final int createId;
    private static AtomicInteger NUM_ID = new AtomicInteger(1);
    private List<Disposable> disposableList = new ArrayList();

    public JsPlugin() {
        createId = NUM_ID.getAndIncrement();
    }

    public final void addDisposable(Disposable disposable) {
        disposableList.add(disposable);
    }

    public final void setExposedJsApi(ExposedJsApi exposedJsApi) {
        mExposedJsApi = exposedJsApi;
    }

    protected final Activity getActivity() {
        return mActivity;
    }

    private void onDestroyDisposable() {
        if (!disposableList.isEmpty()) {
            List<Disposable> copy = new ArrayList(disposableList);
            //
            disposableList.clear();
            for (Disposable disposable : copy) {
                try {
                    disposable.dispose();
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }
        }
    }

    /**
     * 页面加载完成后，需要app主动调用js传递数据
     * WebView的WebViewClient#onPageFinished 页面加载完成
     */
    public void onPageFinished() {
    }

    /**
     * JsPlugin插件创建,初始化资源
     *
     * @param activity
     */
    public void onCreate(Activity activity) {
        L.i(TAG, "onCreate Id=", createId);
        this.mActivity = activity;
    }

    /**
     * JsPlugin插件销毁,释放相关资源
     */
    public void onDestroy() {
        L.i(TAG, "onDestroy Id=", createId);
        mActivity = null;
    }

    /**
     * JsPlugin插件调用startActivityResult后接收回到数据
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        L.i(TAG, "onActivityResult->requestCode=", requestCode, " resultCode=", resultCode);
    }

    protected void applySuccess(String methodName, String msg) {
        JsCallback jsCallback = buildJsCallback();
        jsCallback.applySuccess(methodName, msg);
    }

    protected void applySuccess(String methodName, JSONObject resultData) {
        JsCallback jsCallback = buildJsCallback();
        jsCallback.applySuccess(methodName, resultData);
    }

    protected void applyFail(String methodName, String msg) {
        JsCallback jsCallback = buildJsCallback();
        jsCallback.applyFail(methodName, msg);
    }

    protected void applyFail(String methodName, JSONObject resultData) {
        JsCallback jsCallback = buildJsCallback();
        jsCallback.applyFail(methodName, resultData);
    }

    /**
     * app调用js方法
     *
     * @return
     */
    protected final JsCallback buildJsCallback() {
        JsCallback jsCallback = new JsCallback(mExposedJsApi);
        return jsCallback;
    }

//    /**
//     * App原生调用JS方法-标准方法
//     *
//     * @param jsonData
//     */
//    public final void callJsApplySuccess(String jsonData) {
//        JsCallback jsCallback = buildJsCallback();
//        jsCallback.applySuccess(jsonData);
//    }

    /**
     * App原生调用JS方法-自定义js方法名字
     *
     * @param jsMethod
     * @param jsonData
     */
    public final void callJavaScript(String jsMethod, JSONObject jsonData) {
        JsCallback jsCallback = buildJsCallback();
        jsCallback.applyMethodName(jsMethod, jsonData);
    }


}
