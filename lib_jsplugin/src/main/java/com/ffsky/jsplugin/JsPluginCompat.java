package com.ffsky.jsplugin;

import android.app.Activity;
import android.content.Intent;

import java.util.List;

import androidx.annotation.Nullable;

public final class JsPluginCompat {

    public static final void onPageFinished(List<JsPlugin> list) {
        for (JsPlugin plugin : list) {
            plugin.onPageFinished();
        }
    }

    public static final void create(List<JsPlugin> list, Activity activity,final ExposedJsApi exposedJsApi) {
        for (JsPlugin plugin : list) {
            plugin.setExposedJsApi(exposedJsApi);
            plugin.onCreate(activity);
        }
    }

    public static final void destroy(List<JsPlugin> list) {
        for (JsPlugin plugin : list) {
            plugin.onDestroy();
        }
    }

    public static final void onActivityResult(List<JsPlugin> list, int requestCode, int resultCode, @Nullable Intent data) {
        for (JsPlugin plugin : list) {
            plugin.onActivityResult(requestCode, resultCode, data);
        }
    }
}
