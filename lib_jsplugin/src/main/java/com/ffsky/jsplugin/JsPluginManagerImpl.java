package com.ffsky.jsplugin;

import java.util.HashMap;
import java.util.Map;

class JsPluginManagerImpl extends JsPluginManager {

    private Map<String, Class<? extends JsPlugin>> plugins = new HashMap();

    protected JsPluginManagerImpl() {
        super();
    }

    public void register(String name, Class<? extends JsPlugin> cls) {
        if(!plugins.containsKey(name)){
            plugins.put(name, cls);
        }
    }
    public void unregister(String name) {
        plugins.remove(name);
    }

    public Map<String, Class<? extends JsPlugin>> map() {
        Map<String, Class<? extends JsPlugin>> copy = new HashMap(plugins);
        return copy;
    }

    public void clearPlugin() {
        plugins.clear();
    }

    @Override
    public void dispose() {

    }
}
