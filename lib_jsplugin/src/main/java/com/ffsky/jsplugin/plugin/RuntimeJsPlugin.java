package com.ffsky.jsplugin.plugin;

import android.webkit.JavascriptInterface;

import com.ffsky.jsplugin.JsPlugin;
import com.ffsky.litepack.app.AppConf;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * hybrid runtime appInfo
 */
public class RuntimeJsPlugin extends JsPlugin {

    private static int HybridVersion = 100;

    public static void setHybridVersion(int hybridVersion){
        RuntimeJsPlugin.HybridVersion = hybridVersion;
    }
    @JavascriptInterface
    public String getAppInfo() {
        JSONObject json = new JSONObject();
        try {
            json.put("versionName", AppConf.vName);
            json.put("versionCode", AppConf.vCode);
            json.put("hybridVersion", HybridVersion);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json.toString();
    }
}
