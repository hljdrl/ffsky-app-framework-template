package com.ffsky.jsplugin.plugin;

import android.webkit.JavascriptInterface;

import com.ffsky.jsplugin.JsPlugin;
import com.ffsky.litepack.Lite;
import com.ffsky.litepack.util.StringCompat;


public class CacheJsPlugin extends JsPlugin {

    private final String EXT_KEY = "_H5_";

    @JavascriptInterface
    public void save(String key, String value) {
        Lite.kvCache.save(StringCompat.string(EXT_KEY,key), value);
    }

    @JavascriptInterface
    public String read(String key) {
        return Lite.kvCache.read(StringCompat.string(EXT_KEY,key));
    }

    @JavascriptInterface
    public void del(String key) {
        Lite.kvCache.del(StringCompat.string(EXT_KEY,key));
    }
}
