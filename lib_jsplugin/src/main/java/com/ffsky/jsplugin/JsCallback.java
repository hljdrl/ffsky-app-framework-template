package com.ffsky.jsplugin;

import android.text.TextUtils;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.ffsky.litepack.Lite;
import com.ffsky.litepack.app.AppConf;
import com.ffsky.logger.L;

public final class JsCallback {

    public static final int RESULT_OK = 200;

    public static final int RESULT_ERROR = 0;

    private static String JS_FUNCTION = "javascript:JSBridge._handleMessageFromNative(%s);";

    private static String JS_FUNCTION_METHOD = "javascript:%s(%s);";

    private ExposedJsApi mExposedJsApi;

    public JsCallback(ExposedJsApi exposedJsApi) {
        mExposedJsApi = exposedJsApi;
    }

    /**
     * app原生调用js方法
     *
     * @param methodName js方法名字
     * @param dataJSON   jsonObject数据对象
     */
    public void applyMethodName(String methodName, JSONObject dataJSON) {
        apply(methodName, RESULT_OK, "ok", dataJSON);
    }

    /**
     * 成功回调
     *
     * @param msg
     */
    public final void applySuccess(String methodName, String msg) {
        apply(methodName, RESULT_OK, msg, null);
    }


    /**
     * 成功回调
     *
     * @param msg
     */
    public final void applySuccess(String methodName, String msg,String data) {
        applyString(methodName, RESULT_OK, msg, data);
    }

    /**
     * 成功回调
     *
     * @param methodName
     * @param resultData
     */
    public final void applySuccess(String methodName, JSONObject resultData) {
        apply(methodName, RESULT_OK, "ok", resultData);
    }

    /**
     * 失败回调
     *
     * @param msg
     */
    public final void applyFail(String methodName, String msg) {
        JSONObject _result = new JSONObject();
        apply(methodName, RESULT_ERROR, msg, _result);
    }

    public final void applyFail(String methodName, JSONObject resultData) {
        apply(methodName, RESULT_ERROR, "error", resultData);
    }

    /**
     * "{\"code\":%s,\"msg\":\"%s\",\"result\":%s}"
     */
    private void apply(String js_function, int code, String msg, JSONObject resultData) {
        try {
            //--------------------------------------------------
            JSONObject result = new JSONObject();
            result.put("code", code);
            result.put("msg", msg);
            if (resultData != null) {
                result.put("data", resultData);
            }
            result.put("_app_ver_code", AppConf.vCode);
            result.put("_app_ver_name", AppConf.vName);
            //--------------------------------------------------
            String _send = result.toString();
            String execJs = String.format(JS_FUNCTION_METHOD, js_function, _send);
            callJS(execJs);
        } catch (JSONException e) {
            e.printStackTrace();
            apply(js_function, RESULT_ERROR, e.toString(), new JSONObject());
        }
    }
    private void applyString(String js_function, int code, String msg, String resultData) {
        try {
            //--------------------------------------------------
            JSONObject result = new JSONObject();
            result.put("code", code);
            result.put("msg", msg);
            if (!TextUtils.isEmpty(resultData)) {
                result.put("data", resultData);
            }
            result.put("_app_ver_code", AppConf.vCode);
            result.put("_app_ver_name", AppConf.vName);
            //--------------------------------------------------
            String _send = result.toString();
            String execJs = String.format(JS_FUNCTION_METHOD, js_function, _send);
            callJS(execJs);
        } catch (JSONException e) {
            e.printStackTrace();
            apply(js_function, RESULT_ERROR, e.toString(), new JSONObject());
        }
    }

    /**
     * 自定义回调
     *
     * @param js_function js方法
     * @param dataJSON    回调数据
     */
    public void applyCustom(String js_function, JSONObject dataJSON) {
        String _send = dataJSON.toString();
        String execJs = String.format(JS_FUNCTION_METHOD, js_function, _send);
        callJS(execJs);
    }
    /**
     * 自定义回调
     *
     * @param js_function js方法
     * @param str    回调数据
     */
    public void applyCustom(String js_function, String str) {
        String _send = str;
        String execJs = String.format(JS_FUNCTION_METHOD, js_function, _send);
        callJS(execJs);
    }

    /**
     * webview调用js方法
     *
     * @param js
     */
    private void callJS(final String js) {
        if (mExposedJsApi == null) {
            L.e("JsCallback", "ExposedJsApi Null");
            return;
        }
        final ExposedJsApi jsApi = mExposedJsApi;
        if (jsApi != null) {
            Lite.task.ui(new Runnable() {
                @Override
                public void run() {
                    jsApi.evaluateJavascript(js);
                }
            });
        } else {
            L.e("JsCallback", "ExposedJsApi Null Error");
        }
    }
}
