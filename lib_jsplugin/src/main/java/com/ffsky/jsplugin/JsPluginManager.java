package com.ffsky.jsplugin;

import com.ffsky.jsplugin.plugin.CacheJsPlugin;
import com.ffsky.jsplugin.plugin.RuntimeJsPlugin;
import com.ffsky.litepack.disposable.Disposable;

import java.util.Map;

public abstract class JsPluginManager implements Disposable {
    private static JsPluginManager INSTANCE = null;

    public static JsPluginManager getInstance() {
        if (INSTANCE == null) {
            synchronized (JsPluginManager.class) {
                INSTANCE = new JsPluginManagerImpl();
                INSTANCE.register("cache", CacheJsPlugin.class);
                INSTANCE.register("runtime", RuntimeJsPlugin.class);
            }
        }
        return INSTANCE;
    }

    protected JsPluginManager(){
    }

    public abstract void register(String name, Class<? extends JsPlugin> cls);

    public abstract void unregister(String name);

    public abstract Map<String, Class<? extends JsPlugin>> map();

    public abstract void clearPlugin();


}
