package com.ffsky.litepack.storage;

import android.text.TextUtils;
import android.util.Log;
import android.webkit.MimeTypeMap;

import com.ffsky.litepack.util.StringCompat;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class FileUtil {
    private static final String TAG = "FileUtil";

    public static List<String> filterFiles(File file, File towFile) {
        String fileName = file.getName();
        String _twoFileName=null;
        if(towFile!=null){
            _twoFileName = towFile.getName();
        }
        List<String> mLm = new ArrayList<>();
        File filedir = file.getParentFile();
        if (filedir.exists()) {
            if (filedir.isDirectory()) {
                String[] children = filedir.list();
                if (children != null) {
                    for (int i = 0; i < children.length; i++) {
                        File _file = new File(filedir, children[i]);
                        if (!TextUtils.equals(fileName, _file.getName()) &&
                                !TextUtils.equals(_twoFileName,_file.getName())) {
                            mLm.add(_file.toString());
                        }
                    }
                }
            }
        }
        return mLm;
    }

    private static void deleteFileDir(File filedir) {
        if (filedir.exists()) {
            if (filedir.isDirectory()) {
                String[] children = filedir.list();
                if (children != null) {
                    for (int i = 0; i < children.length; i++) {
                        File _file = new File(filedir, children[i]);
                        if(_file.isDirectory()){

                        }else{
                            _file.delete();
                        }
                    }
                }
            }
        }
    }


    public static void deleteDirectory(String path) {
        if (!TextUtils.isEmpty(path)) {
            File _dir = new File(path);
            deleteFileDir(_dir);
            _dir.delete();
        }
    }

    public static void forceDelete(String folderPath) {
        try {
            forceDeleteFileImpl(folderPath); //删除完里面所有内容
            String filePath = folderPath;
            File _del_file = new File(filePath);
            _del_file.delete(); //删除空文件夹
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static boolean forceDeleteFileImpl(String path) {
        boolean flag = false;
        File file = new File(path);
        if (!file.exists()) {
            return flag;
        }
        if (!file.isDirectory()) {
            return flag;
        }
        String[] tempList = file.list();
        File temp = null;
        for (int i = 0; i < tempList.length; i++) {
            if (path.endsWith(File.separator)) {
//                temp = new File(path + tempList[i]);
                temp = new File(StringCompat.string(path ,tempList[i]));
            } else {
                temp = new File(StringCompat.string(path , File.separator ,tempList[i]));
            }
            if (temp.isFile()) {
                temp.delete();
            }
            if (temp.isDirectory()) {
                final String _tempPath = StringCompat.string(path, File.separator,tempList[i]);
                forceDeleteFileImpl(_tempPath);//先删除文件夹里面的文件
                forceDelete(_tempPath);//再删除空文件夹
                flag = true;
            }
        }
        return flag;
    }


    public static void newFiles(File _file) {
        if (_file != null) {
            _file.mkdirs();
        }
    }

    public static void createNewFiles(String path) {
        if (!TextUtils.isEmpty(path)) {
            File _file = new File(path);
            deleteFileDir(new File(path));
            _file.mkdirs();
        }
    }

    public static boolean hasFile(File _file) {
        if (_file.exists() && _file.canRead()) {
            long _size = _file.length();
            if (_size <= 20) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    public static boolean hasFile(File _file, boolean checkFile) {
        if (_file.exists() && _file.canRead()) {
            if (checkFile) {
                if (_file.isFile()) {
                    long _size = _file.length();
                    if (_size <= 20) {
                        return false;
                    } else {
                        return true;
                    }
                } else {
                    return false;
                }
            }
            long _size = _file.length();
            if (_size <= 20) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    public static void createNewFile(String path) {
        if (!TextUtils.isEmpty(path)) {
            File mFm = new File(path);
            try {
                mFm.createNewFile();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void delFile(String var0) {
        if (!TextUtils.isEmpty(var0)) {
            File _delFile = new File(var0);
            if(_delFile.exists()){
                _delFile.delete();
            }
        }
    }

    public static boolean hasExtentsion(String filename) {
        int dot = filename.lastIndexOf('.');
        if ((dot > -1) && (dot < (filename.length() - 1))) {
            return true;
        } else {
            return false;
        }
    }

    // 获取文件扩展名
    public static String getExtensionName(String filename) {
        if ((filename != null) && (filename.length() > 0)) {
            int dot = filename.lastIndexOf('.');
            if ((dot > -1) && (dot < (filename.length() - 1))) {
                String var = filename.substring(dot + 1);
                if(var.indexOf('?')>-1) {
                    return var.substring(0,var.indexOf('?'));
                }else {
                    return filename.substring(dot + 1);
                }
            }
        }
        return "";
    }

    // 获取文件名
    public static String getFileNameFromPath(String filepath) {
        if ((filepath != null) && (filepath.length() > 0)) {
            int sep = filepath.lastIndexOf('/');
            if ((sep > -1) && (sep < filepath.length() - 1)) {
                return filepath.substring(sep + 1);
            }
        }
        return filepath;
    }

    // 获取不带扩展名的文件名
    public static String getFileNameNoEx(String filename) {
        if ((filename != null) && (filename.length() > 0)) {
            int dot = filename.lastIndexOf('.');
            if ((dot > -1) && (dot < (filename.length()))) {
                return filename.substring(0, dot);
            }
        }
        return filename;
    }

    public static String getMimeType(String filePath) {
        if (TextUtils.isEmpty(filePath)) {
            return "";
        }
        String type = null;
        String extension = getExtensionName(filePath.toLowerCase());
        if (!TextUtils.isEmpty(extension)) {
            MimeTypeMap mime = MimeTypeMap.getSingleton();
            type = mime.getMimeTypeFromExtension(extension);
        }
        Log.i(TAG, "url:" + filePath + " " + "type:" + type);

        // FIXME
        if (TextUtils.isEmpty(type) && filePath.endsWith("aac")) {
            type = "audio/aac";
        }

        return type;
    }

    public enum SizeUnit {
        Byte,
        KB,
        MB,
        GB,
        TB,
        Auto,
    }

    public static String formatFileSize(long size) {
        return formatFileSize(size, SizeUnit.Auto);
    }

    public static String formatFileSize(long size, SizeUnit unit) {
        if (size < 0) {
            //<string name="unknow_size">未知大小</string>
            return "未知大小";
        }

        final double KB = 1024;
        final double MB = KB * 1024;
        final double GB = MB * 1024;
        final double TB = GB * 1024;
        if (unit == SizeUnit.Auto) {
            if (size < KB) {
                unit = SizeUnit.Byte;
            } else if (size < MB) {
                unit = SizeUnit.KB;
            } else if (size < GB) {
                unit = SizeUnit.MB;
            } else if (size < TB) {
                unit = SizeUnit.GB;
            } else {
                unit = SizeUnit.TB;
            }
        }

        switch (unit) {
            case Byte:
                return StringCompat.string(size,"B");
            case KB:
                return String.format(Locale.US, "%.2fKB", size / KB);
            case MB:
                return String.format(Locale.US, "%.2fMB", size / MB);
            case GB:
                return String.format(Locale.US, "%.2fGB", size / GB);
            case TB:
                return String.format(Locale.US, "%.2fPB", size / TB);
            default:
                return size + "B";
        }
    }


}
