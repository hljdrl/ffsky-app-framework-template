package com.ffsky.litepack.dialog;

import android.app.Dialog;

/**
 * Created by hljdrl on 17/3/15.
 */

public interface OnNegativeListener {

    void onClick(Dialog dialog);
}
