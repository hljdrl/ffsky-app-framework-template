package com.ffsky.litepack.dialog;

import android.app.Dialog;

/**
 * Created by hljdrl on 17/3/15.
 */

public interface OnPositiveListener {

    void onClick(Dialog dialog);
}
