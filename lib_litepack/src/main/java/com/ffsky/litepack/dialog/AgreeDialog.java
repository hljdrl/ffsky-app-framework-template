package com.ffsky.litepack.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.os.Bundle;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.ffsky.litepack.R;
import com.ffsky.litepack.util.DisplayUtil;

import androidx.core.content.ContextCompat;

/**
 * @author hljdrl
 */
public class AgreeDialog extends Dialog implements View.OnClickListener {


    private View mBtnGroupView, mBkgView, mDialogView;

    private TextView mTitleTv, mContentTv, mPositiveBtn, mNegativeBtn;

    private Drawable mDrawable;

    private AnimationSet mAnimIn, mAnimOut;

    private int mResId, mBackgroundColor, mTitleTextColor, mContentTextColor;

    private OnPositiveListener mPositiveListener;

    private OnNegativeListener mNegativeListener;

    private CharSequence mTitleText, mContentText, mPositiveText, mNegativeText;
    private Spanned mContentSpanned;
    private int titleShow = View.VISIBLE;
    private int messageGravity = Gravity.LEFT;
    private int titleTextSize;
    private boolean mIsShowAnim;
    private Typeface titleTypeface;

    public AgreeDialog(Context context) {
        this(context, R.style.color_agree_dialog);
    }

    public AgreeDialog(Context context, int theme) {
        super(context, theme);
        init();
    }


    private void callDismiss() {
        super.dismiss();
    }

    private void init() {
        mAnimIn = AnimationLoader.getInAnimation(getContext());
        mAnimOut = AnimationLoader.getOutAnimation(getContext());
        initAnimListener();
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitleText = title;
    }

    @Override
    public void setTitle(int titleId) {
        setTitle(getContext().getText(titleId));
    }

    private int autoLink;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View contentView = View.inflate(getContext(), R.layout.layout_agree_dialog, null);
        setContentView(contentView);

        mDialogView = getWindow().getDecorView().findViewById(android.R.id.content);
        mBkgView = contentView.findViewById(R.id.llBkg);
        mTitleTv = contentView.findViewById(R.id.tvTitle);

        mContentTv = contentView.findViewById(R.id.tvContent);

        mPositiveBtn = contentView.findViewById(R.id.btnPositive);
        mNegativeBtn = contentView.findViewById(R.id.btnNegative);

        mBtnGroupView = contentView.findViewById(R.id.llBtnGroup);
        if (autoLink != 0) {
            mContentTv.setAutoLinkMask(autoLink);
        }
        //
        mPositiveBtn.setOnClickListener(this);
        mNegativeBtn.setOnClickListener(this);

        mTitleTv.setText(mTitleText);
        mTitleTv.setVisibility(titleShow);
        //
        if (mContentSpanned != null) {
            mContentTv.setText(mContentSpanned);
            mContentTv.setMovementMethod(LinkMovementMethod.getInstance());
        } else {
            mContentTv.setText(mContentText);
        }
        mContentTv.setGravity(messageGravity);

        mPositiveBtn.setText(mPositiveText);
        mNegativeBtn.setText(mNegativeText);
        //
        mPositiveBtn.setTextColor(ContextCompat.getColorStateList
                (getContext(), mPositiveTextColorRes));

        mNegativeBtn.setTextColor(ContextCompat.getColorStateList
                (getContext(), mNegativeTextColorRes));

        if (null == mPositiveListener && null == mNegativeListener) {
            mBtnGroupView.setVisibility(View.GONE);
        } else if (null == mPositiveListener && null != mNegativeListener) {
            mPositiveBtn.setVisibility(View.GONE);
            mNegativeBtn.setBackgroundResource(mNegativeRes);
        } else if (null != mPositiveListener && null == mNegativeListener) {
            mNegativeBtn.setVisibility(View.GONE);
            mPositiveBtn.setBackgroundResource(mPositiveRes);
        } else if (null != mPositiveListener && null != mNegativeListener) {
            mPositiveBtn.setBackgroundResource(mPositiveRes);
            mNegativeBtn.setBackgroundResource(mNegativeRes);
        }
        getWindow().setNavigationBarColor(Color.WHITE);
        setTextColor();
        setBackgroundColor();
        setContentMode();
    }

    //right textColor
    private int mNegativeTextColorRes = R.color.color_blue_light_selector;
    //left textColor
    private int mPositiveTextColorRes = R.color.color_blue_light_selector;
    //right Button
    private int mNegativeRes = R.drawable.selector_dialog_gray_right;
    //left Button
    private int mPositiveRes = R.drawable.selector_dialog_gray_left;

    public void setNegativeRes(int res) {
        mNegativeRes = res;
    }

    public void setPositiveRes(int res) {
        mPositiveRes = res;
    }

    public void setNegativeTextColor(int res) {
        mNegativeTextColorRes = res;
    }

    public void setPositiveTextColor(int res) {
        mPositiveTextColorRes = res;
    }

    public void setTitleVisibility(int _view) {
        titleShow = _view;
        if (mTitleTv != null) {
            mTitleTv.setVisibility(_view);
        }
    }

    /**
     * @param gravity
     */
    public void setMessageGravity(int gravity) {
        messageGravity = gravity;
        if (mContentTv != null) {
            mContentTv.setGravity(messageGravity);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        startWithAnimation(mIsShowAnim);
    }

    @Override
    public void dismiss() {
        dismissWithAnimation(mIsShowAnim);
    }

    private void startWithAnimation(boolean showInAnimation) {
        if (showInAnimation) {
            mDialogView.startAnimation(mAnimIn);
        }
    }

    private void dismissWithAnimation(boolean showOutAnimation) {
//        if (showOutAnimation) {
//            mDialogView.startAnimation(mAnimOut);
//        } else {
        super.dismiss();
//        }
    }

    private void initAnimListener() {
        mAnimOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mDialogView.post(new Runnable() {
                    @Override
                    public void run() {
                        callDismiss();
                    }
                });
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
    }

    private void setBackgroundColor() {

        if (0 == mBackgroundColor) {
            return;
        }

        int radius = DisplayUtil.dp2px(6);
        float[] outerRadii = new float[]{radius, radius, radius, radius, 0, 0, 0, 0};
        RoundRectShape roundRectShape = new RoundRectShape(outerRadii, null, null);
        ShapeDrawable shapeDrawable = new ShapeDrawable(roundRectShape);
        shapeDrawable.getPaint().setColor(mBackgroundColor);
        shapeDrawable.getPaint().setStyle(Paint.Style.FILL);
        mBkgView.setBackground(shapeDrawable);
    }

    private void setTextColor() {

        if (0 != mTitleTextColor) {
            mTitleTv.setTextColor(mTitleTextColor);
        }
        if (0 != titleTextSize) {
            mTitleTv.setTextSize(TypedValue.COMPLEX_UNIT_SP, titleTextSize);
        }
        if (titleTypeface != null) {
            mTitleTv.setTypeface(titleTypeface);
        }

        if (0 != mContentTextColor) {
            mContentTv.setTextColor(mContentTextColor);
        }
    }

    private void setContentMode() {
        boolean isImageMode = (null != mDrawable | 0 != mResId);
        boolean isTextMode = (!TextUtils.isEmpty(mContentText));

        if (isImageMode && isTextMode) {
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) mContentTv.getLayoutParams();
            params.gravity = Gravity.BOTTOM;
            mContentTv.setLayoutParams(params);
            mContentTv.setBackgroundColor(Color.BLACK);
            mContentTv.getBackground().setAlpha(0x28);
            mContentTv.setVisibility(View.VISIBLE);
            return;
        }

        if (isTextMode) {
            mContentTv.setVisibility(View.VISIBLE);
            return;
        }

        if (isImageMode) {
            mContentTv.setVisibility(View.GONE);
            return;
        }

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (R.id.btnPositive == id) {
            mPositiveListener.onClick(this);
        } else if (R.id.btnNegative == id) {
            mNegativeListener.onClick(this);
        } else {
        }
    }

    public AgreeDialog setAnimationEnable(boolean enable) {
        mIsShowAnim = enable;
        return this;
    }

    public AgreeDialog setAnimationIn(AnimationSet animIn) {
        mAnimIn = animIn;
        return this;
    }

    public AgreeDialog setAnimationOut(AnimationSet animOut) {
        mAnimOut = animOut;
        initAnimListener();
        return this;
    }

    public AgreeDialog setColor(int color) {
        mBackgroundColor = color;
        return this;
    }

    public AgreeDialog setTitleTypeface(Typeface typeface) {
        this.titleTypeface = typeface;
        return this;
    }

    public AgreeDialog setTitleTextSize(int size) {
        titleTextSize = size;
        return this;
    }

    public AgreeDialog setColor(String color) {
        try {
            setColor(Color.parseColor(color));
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        return this;
    }

    public AgreeDialog setTitleTextColor(int color) {
        mTitleTextColor = color;
        return this;
    }

    public AgreeDialog setTitleTextColor(String color) {
        try {
            setTitleTextColor(Color.parseColor(color));
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        return this;
    }

    public AgreeDialog setContentTextColor(int color) {
        mContentTextColor = color;
        return this;
    }

    public AgreeDialog setContentTextColor(String color) {
        try {
            setContentTextColor(Color.parseColor(color));
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        return this;
    }


    public AgreeDialog setPositiveListener(CharSequence text, OnPositiveListener l) {
        mPositiveText = text;
        mPositiveListener = l;
        return this;
    }

    public AgreeDialog setPositiveListener(int textId, OnPositiveListener l) {
        return setPositiveListener(getContext().getText(textId), l);
    }

    public AgreeDialog setNegativeListener(CharSequence text, OnNegativeListener l) {
        mNegativeText = text;
        mNegativeListener = l;
        return this;
    }

    public AgreeDialog setNegativeListener(int textId, OnNegativeListener l) {
        return setNegativeListener(getContext().getText(textId), l);
    }

    public AgreeDialog setContentText(CharSequence text) {
        mContentText = text;
        return this;
    }

    public AgreeDialog setContentText(Spanned text) {
        mContentSpanned = text;
        return this;
    }

    public AgreeDialog setContentText(int textId) {
        return setContentText(getContext().getText(textId));
    }

    public AgreeDialog setContentImage(Drawable drawable) {
        mDrawable = drawable;
        return this;
    }

    public AgreeDialog setContentImage(int resId) {
        mResId = resId;
        return this;
    }

    public CharSequence getContentText() {
        return mContentText;
    }

    public CharSequence getTitleText() {
        return mTitleText;
    }

    public CharSequence getPositiveText() {
        return mPositiveText;
    }

    public CharSequence getNegativeText() {
        return mNegativeText;
    }

    public void setAutoLink(int autoLink) {
        this.autoLink = autoLink;
    }
}
