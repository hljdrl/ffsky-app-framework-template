package com.ffsky.litepack.util;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.ColorRes;

/**
 *
 */

public class ActivityUtil {

    public static boolean isDarkModeStatus(Context context) {
        Resources res = context.getResources();
        int mode = res.getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK;
        try {
            if (mode == Configuration.UI_MODE_NIGHT_YES) {
                return true;
            }
        } catch (Exception ex) {
        }
        return false;
    }

    public final static boolean isDestroyed(Activity _activity) {
        if (_activity == null || _activity.isDestroyed() || _activity.isFinishing()) {
            return true;
        }
        return false;
    }

    /**
     * 状态栏颜色设置
     *
     * @param context
     */
    public static void setStatusBar(Activity context, @ColorRes int color) {
        Window window = context.getWindow();
        Resources resources = context.getResources();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(resources.getColor(color, resources.newTheme()));
    }

    /**
     * 设置标题框透明
     *
     * @param context
     */
    public static void setStatusBar(Activity context) {
        Window window = context.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(Color.TRANSPARENT);
    }

}
