package com.ffsky.litepack.util;

import android.text.TextUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 */
public class StringCompat {

    public static String formatLog(String log, String regex) {
        if (!TextUtils.isEmpty(log)) {
            return log.replaceAll(regex, "***");
        }
        return log;
    }

    public static String string(String... str) {
        StringBuffer _buf = new StringBuffer();
        if (str != null) {
            for (String _s : str) {
                _buf.append(_s);
            }
        }
        return _buf.toString();
    }

    public static String string(Object... str) {
        StringBuffer _buf = new StringBuffer();
        if (str != null) {
            for (Object _s : str) {
                if (_s != null) {
                    _buf.append(_s.toString());
                }
            }
        }
        return _buf.toString();
    }

    public final static boolean isHttp(String var) {
        if (TextUtils.isEmpty(var)) {
            return Boolean.FALSE;
        }
        if (var.startsWith("http")) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    public static final String value(String str, String defaultValue) {
        if (TextUtils.isEmpty(str)) {
            return defaultValue;
        }
        return str;
    }

    public static boolean isNumeric(String str) {
        Pattern pattern = Pattern.compile("-?[0-9]+.?[0-9]+");
        Matcher isNum = pattern.matcher(str);
        if (!isNum.matches()) {
            return false;
        }
        return true;
    }

    /**
     * 大陆手机号码11位数，匹配格式：前三位固定格式+后8位任意数
     * 此方法中前三位格式有：
     * 13+任意数
     * 15+除4的任意数
     * 18+除1和4的任意数
     * 17+除9的任意数
     * 147
     */
    public static boolean isChinaPhoneLegal(String str) {
        String regExp = "^((13[0-9])|(15[^4])|(18[0,1,2,3,4,5-9])|(17[0-8])|(147))\\d{8}$";
        Pattern p = Pattern.compile(regExp);
        Matcher m = p.matcher(str);
        return m.matches();
    }

    /**
     * 手机号判断
     *
     * @param _number
     * @return
     */
    public static boolean matchPhoneNumber(String _number) {
        /*
         * 移动: 2G号段(GSM网络)有139,138,137,136,135,134,159,158,152,151,150,
         * 3G号段(TD-SCDMA网络)有157,182,183,188,187 147是移动TD上网卡专用号段. 联通:
         * 2G号段(GSM网络)有130,131,132,155,156 3G号段(WCDMA网络)有186,185 电信:
         * 2G号段(CDMA网络)有133,153 3G号段(CDMA网络)有189,180
         */
        String YD = "^[1]{1}(([3]{1}[4-9]{1})|([5]{1}[012789]{1})|([8]{1}[2378]{1})|([4]{1}[7]{1}))[0-9]{8}$";
        String LT = "^[1]{1}(([3]{1}[0-2]{1})|([5]{1}[56]{1})|([8]{1}[56]{1}))[0-9]{8}$";
        String DX = "^[1]{1}(([3]{1}[3]{1})|([5]{1}[3]{1})|([8]{1}[09]{1}))[0-9]{8}$";
        /**
         * flag = 1 YD 2 LT 3 DX
         */
        boolean flag = false;// 存储匹配结果
        // 判断手机号码是否是11位
        if (_number.length() == 11) {
            // 判断手机号码是否符合中国移动的号码规则
            if (_number.matches(YD)) {
                flag = true;
            }
            // 判断手机号码是否符合中国联通的号码规则
            else if (_number.matches(LT)) {
                flag = true;
            }
            // 判断手机号码是否符合中国电信的号码规则
            else if (_number.matches(DX)) {
                flag = true;
            }
            // 都不合适 未知
        }
        return flag;
    }

}
