package com.ffsky.litepack.util;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;

import com.ffsky.litepack.Lite;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 */

public class DataUtil {

    public static final String getString(int resId,String time){
        Context context = Lite.getContext();
        Resources res = context.getResources();
        return res.getString(resId,time);
    }

    public static final String fileName(long time) {
        Date _date = new Date(time);
        DateFormat datetimeDf = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
        return datetimeDf.format(_date);
    }

    public static final String makeTime() {
        Date _date = Calendar.getInstance().getTime();
        DateFormat datetimeDf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return datetimeDf.format(_date);
    }

    public static final String makeTimeHour(long time) {
        Date _date = new Date(time);
        DateFormat datetimeDf = new SimpleDateFormat("HH:mm");
        return datetimeDf.format(_date);
    }

    public static final String logTime(Date _date) {
        DateFormat datetimeDf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return datetimeDf.format(_date);
    }

    public static final String makeTime(long time) {
        Date _date = new Date(time);
        DateFormat datetimeDf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return datetimeDf.format(_date);
    }

    public static final String makeTimeDay() {
        Date _date = new Date(System.currentTimeMillis());
        DateFormat datetimeDf = new SimpleDateFormat("yyyyMMdd");
        return datetimeDf.format(_date);
    }


    public static boolean isDay(Date date) {
        if (date == null) {
            return false;
        }
        // 白天
        // 早上6点,晚上6点
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        //
        int _hour = cal.get(Calendar.HOUR_OF_DAY);
        if (_hour >= 6 && _hour <= 17) {
            return true;
        }
        return false;
    }

    public static boolean isNight(Date date) {
        //夜晚
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        //
        int _hour = cal.get(Calendar.HOUR_OF_DAY);
        if (_hour < 6 && _hour > 18) {
            return true;
        }
        return false;
    }

    public static Date toDate(String time) {
        if (TextUtils.isEmpty(time)) {
            return null;
        }
        StringBuffer _buf = new StringBuffer();
        _buf.append(time);
        if (time.length() == 10) {
            _buf.append("000");
        }
        Date _date = new Date(Long.parseLong(_buf.toString()));
        return _date;
    }

    public static long toTime(String time, long addTime) {
        if (TextUtils.isEmpty(time)) {
            return 0;
        }
        StringBuffer _buf = new StringBuffer();
        _buf.append(time);
        if (time.length() == 10) {
            _buf.append("000");
        }
        Date _date = new Date(Long.parseLong(_buf.toString()) + addTime);
        return _date.getTime();
    }

}

