package com.ffsky.litepack.util;



import android.content.Context;

import com.ffsky.logger.L;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class ClassCompat {

    public static Map<String, Object> forValue(Class clazz) {
        Map<String,Object> map = new HashMap(16);
        Field[] fields = clazz.getFields();
        for (Field field : fields) {
            Class<?> type = field.getType();
            Object value = null;
            try {
                value = field.get(type);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            map.put(field.getName(),value);
        }
        return map;
    }

    public static Object getBuildConfigValue(Context ctx, String key)
    {
        try
        {
            Class<?> clazz = Class.forName(ctx.getClass().getPackage().getName() + ".BuildConfig");
            Field field = clazz.getField(key);
            return field.get(null);
        } catch (ClassNotFoundException e) {
            L.d("ClassCompat", "Unable to get the BuildConfig, is this built with ANT?");
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            L.d("ClassCompat", key + " is not a valid field. Check your build.gradle");
        } catch (IllegalAccessException e) {
            L.d("ClassCompat", "Illegal Access Exception: Let's print a stack trace.");
            e.printStackTrace();
        } catch (NullPointerException e) {
            L.d("ClassCompat", "Null Pointer Exception: Let's print a stack trace.");
            e.printStackTrace();
        }

        return null;
    }
}
