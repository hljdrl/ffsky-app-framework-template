package com.ffsky.litepack.toolbar;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ffsky.litepack.R;
import com.ffsky.litepack.disposable.Disposable;
import com.ffsky.logger.L;

import androidx.annotation.DrawableRes;
import androidx.annotation.StringRes;

/**
 * 标题栏
 */
public final class Toolbar implements Disposable {

    public static Toolbar build(View view) {
        Toolbar toolbar = new Toolbar(view);
        //
        return toolbar;
    }

    public static Toolbar build(Activity activity) {
        Toolbar toolbar = new Toolbar(activity.findViewById(R.id.tool_bar));
        return toolbar;
    }

    final String TAG = "Toolbar";
    private final View mView;
    private ImageView backImageView;
    private TextView titleView;
    private TextView rightTextView;
    private Toolbar(View toolBarView) {
        mView = toolBarView;
        if (mView != null) {
            backImageView = mView.findViewById(R.id.toolbar_back);
            titleView = mView.findViewById(R.id.toolbar_title);
            rightTextView = mView.findViewById(R.id.toolbar_right);
            if (backImageView != null && titleView != null) {
                L.i(TAG, "Toolbar Init OK");
            }
        } else {
            L.i(TAG, "Toolbar Error Not find Layout res");
        }
    }
    public void hideBack(){
        updateViewState(backImageView,View.GONE);
    }
    public void showBack(){
        updateViewState(backImageView,View.VISIBLE);
    }

    public void hideRight(){
        updateViewState(rightTextView,View.GONE);
    }
    public void showRight(){
        updateViewState(rightTextView,View.VISIBLE);
    }

    void updateViewState(View v,int showAndHide){
        if(v!=null){
            v.setVisibility(showAndHide);
        }
    }

    /**
     * 右侧按钮TextView
     * @return
     */
    public TextView getRightTextView(){
        return rightTextView;
    }
    /**
     * 返回左侧按钮组件
     * @return
     */
    public ImageView getBackImageView(){
        return backImageView;
    }
    /**
     * 标题组件
     * @return
     */
    public TextView getTitleView(){
        return titleView;
    }
    public void setTitle(String title) {
        if (titleView != null) {
            titleView.setText(title);
        }
    }
    public void setTitle(@StringRes int title) {
        if (titleView != null) {
            titleView.setText(title);
        }
    }
    public void setTitleColor(int color){
        if(titleView!=null){
            titleView.setTextColor(color);
        }
    }
    public void setTitleSize(int size){
        if(titleView!=null){
            titleView.setTextSize(size);
        }
    }
    public void setBackground(Drawable drawable){
        if(mView!=null){
            mView.setBackground(drawable);
        }
    }
    public void setBackgroundColor(int color){
        if(mView!=null){
            mView.setBackgroundColor(color);
        }
    }
    public void setBackgroundResource(@DrawableRes int resid){
        if(mView!=null){
            mView.setBackgroundResource(resid);
        }
    }
    public View getView(){
        return mView;
    }

    public void setBackOnClickListener(View.OnClickListener onClickListener) {
        if (backImageView != null) {
            backImageView.setOnClickListener(onClickListener);
        }
    }
    public void setRightOnClickListener(View.OnClickListener onClickListener){
        if(rightTextView!=null){
            rightTextView.setOnClickListener(onClickListener);
        }
    }
    public void show(){
        updateViewState(mView,View.VISIBLE);
    }
    public void hide(){
        updateViewState(mView,View.GONE);
    }

    @Override
    public void dispose() {
        if (backImageView != null) {
            backImageView.setOnClickListener(null);
        }
        if(rightTextView!=null){
            rightTextView.setOnClickListener(null);
        }
    }
}
