package com.ffsky.litepack.listener;

/**
 * @author drl
 */
public interface OnListener<T> {

    int CODE_OK = 200;
    int CODE_ERROR = -1;

    /**
     * 成功
     * @param var1
     */
    void onSuccess(T var1);

    /**
     * 失败
     * @param code
     * @param error
     */
    void onFailure(int code, String error);
}
