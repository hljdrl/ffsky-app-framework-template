package com.ffsky.litepack.listener;

/**
 * @author drl
 */
public interface LiteBack {

    /**
     * 成功
     */
    void onSuccess();

    /**
     * 失败
     * @param code
     * @param error
     */
    void onFailure(int code, String error);
}
