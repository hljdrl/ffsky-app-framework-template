package com.ffsky.litepack.cache;

import android.text.TextUtils;

import com.ffsky.litepack.dao.KeyValueTable;

/**
 *
 */

public class KVTableCache implements KVCache {

    KeyValueTable table;

    public KVTableCache() {
        table = KeyValueTable.getInstance();
    }

    @Override
    public void save(String k, String v) {
        table.writeItem(k, v);
    }

    @Override
    public String read(String k) {
        return table.readItem(k, null);
    }

    @Override
    public void del(String k) {
        table.deleteItem(k);
    }

    @Override
    public String read(String k, String value) {
        return table.readItem(k, value);
    }

    @Override
    public int readToInt(String k, int value) {
        String _mSm = read(k, String.valueOf(value));
        if (TextUtils.isEmpty(_mSm)) {
            return value;
        }
        try {
            int _mIm = Integer.parseInt(_mSm);
            return _mIm;
        } catch (Exception ex) {
        }
        return value;
    }

    @Override
    public void clear() {
        table.clear();
    }

}
