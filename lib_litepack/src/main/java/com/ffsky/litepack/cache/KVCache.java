package com.ffsky.litepack.cache;

/**
 * Created by hljdrl on 17/3/23.
 */

public interface KVCache {

     void save(String k, String v);

     String read(String k);

     void del(String k);

     String read(String k, String value);

     int readToInt(String k, int value);

     void clear();
}
