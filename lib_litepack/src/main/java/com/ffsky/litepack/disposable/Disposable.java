package com.ffsky.litepack.disposable;

public interface Disposable {

    void dispose ();

}
