package com.ffsky.litepack.app;


import android.app.Activity;

import java.lang.ref.WeakReference;

/**
 * App行为操作
 * @author hljdrl
 */
public interface AppInterface {

    /**
     * 关闭app
     */
    void closeApp();

    /**
     * 关闭所有activity
     */
    void closeAllActivity();

    /**
     * 获得当前显示Activity,可能null空，请使用这做好判断.
     * @return
     */
    WeakReference<Activity> getCurrentActivity();
    /**
     * 关闭除了主页home页面的其它所有二级Activity页面
     * @param closeSecondAllActivity
     *
     */
    void goAppHome(boolean closeSecondAllActivity);




    /**
     *  跳转到登录页
     * @param activity
     */
    void goLogin(Activity activity);

    /**
     * 跳转到关乎页面
     * @param activity
     */
    void goAbout(Activity activity);


    /**
     * 用户同意隐私协议
     */
     void agreePrivacyAgreement();

}
