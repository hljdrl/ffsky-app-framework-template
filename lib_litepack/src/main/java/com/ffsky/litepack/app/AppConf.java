package com.ffsky.litepack.app;

/**
 * App全局唯一配置,app版本信息，app模块BuildConfig配置文件
 */
public abstract class AppConf {

    public static boolean debug = false;
    public static String pkg;
    public static String vName;
    public static int vCode;
    public static boolean appInit = false;

    /**
     * @param debug apk包debug开关
     * @param pkg apk的applicationId
     * @param vcode apk的VersionCode
     * @param vName apk的VersionName
     * @param env  apk渠道打包env环境配置: dev、pro
     */
    public AppConf(boolean debug, String pkg, int vcode, String vName, String env) {
        AppConf.debug = debug;
        AppConf.pkg = pkg;
        AppConf.vCode = vcode;
        AppConf.vName = vName;
    }

    /**
     * 获取 app模块BuildConfig文件内容
     *
     * @param key
     * @return
     */
    public abstract String conf(String key);

}
