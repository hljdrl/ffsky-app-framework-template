package com.ffsky.litepack.task;

public interface Task {
    void runTask();
}
