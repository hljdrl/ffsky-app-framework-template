package com.ffsky.litepack.task;



import com.ffsky.litepack.util.StringCompat;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 */

public class TaskFactoryImpl implements ThreadFactory {
    private final AtomicInteger threadNumber = new AtomicInteger(1);
    @Override
    public Thread newThread(Runnable r) {
        Thread t = new Thread(r);
        t.setName(StringCompat.string("SKY-", String.valueOf(threadNumber.getAndIncrement())));
        t.setPriority(Thread.MIN_PRIORITY);
        return t;
    }
}
