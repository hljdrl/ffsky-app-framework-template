package com.ffsky.litepack.task;

public interface SkyTask {

    void post(Runnable run);

    void postQueue(Runnable run);

    void postQueueTask(Task task);

    void ui(Runnable run);

    void uiTime(Runnable run, long time);

    void uiRemove(Runnable run);

    void destroy();
}
