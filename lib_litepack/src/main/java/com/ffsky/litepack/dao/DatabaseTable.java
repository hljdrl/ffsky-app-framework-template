package com.ffsky.litepack.dao;


import net.sqlcipher.database.SQLiteDatabase;

interface DatabaseTable {

	/**
	 * Called on create database.
	 * 
	 * @param db
	 */
	void create(SQLiteDatabase db);

	/**
	 * Called on database migration.
	 * 
	 * @param db
	 * @param toVersion
	 */
	void migrate(SQLiteDatabase db, int toVersion);

	/**
	 * Called on clear database request.
	 */
	void clear();
	
	

}
