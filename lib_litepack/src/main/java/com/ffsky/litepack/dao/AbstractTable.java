package com.ffsky.litepack.dao;


import net.sqlcipher.Cursor;
import net.sqlcipher.database.SQLiteDatabase;

public  abstract class AbstractTable implements DatabaseTable {

	protected abstract String getTableName();

	protected abstract String[] getProjection();
	

	protected String getListOrder() {
		return null;
	}

	@Override
	public void migrate(SQLiteDatabase db, int toVersion) {
		DataBaseManager.dropTable(db, getTableName());
	}

	/**
	 * Query table.
	 * 
	 * @return Result set with defined projection and in defined order.
	 */
	public Cursor list() {
		SQLiteDatabase db = DataBaseManager.getInstance().getWritableDatabase();
		return db.query(getTableName(), getProjection(), null, null, null,
				null, getListOrder());
	}
	public int getCount(){
		int count = 0;
		Cursor _cursor = list();
		if(_cursor!=null){
			count = _cursor.getCount();
		}
		closeCurosr(_cursor);
		return count;
	}
	
	public Cursor list(String select,String[] selectionArgs) {
		SQLiteDatabase db = DataBaseManager.getInstance().getWritableDatabase();
		return db.query(getTableName(), getProjection(), select, selectionArgs, null,
				null, getListOrder());
	}
	public Cursor list(String select,String[] selectionArgs,String groupBy) {
		SQLiteDatabase db = DataBaseManager.getInstance().getWritableDatabase();
		return db.query(getTableName(), getProjection(), select, selectionArgs, groupBy,
				null, getListOrder());
	}
	public Cursor list(String select,String[] selectionArgs,String groupBy,String order) {
		SQLiteDatabase db = DataBaseManager.getInstance().getWritableDatabase();
		return db.query(getTableName(), getProjection(), select, selectionArgs, groupBy,
				null, order);
	}

	@Override
	public void clear() {
		SQLiteDatabase db = DataBaseManager.getInstance().getWritableDatabase();
		db.delete(getTableName(), null, null);
	}
	protected boolean hasData(Cursor c){
		if(c!=null && c.getCount()>0){
			return true;
		}
		return false;
	}
	protected void closeCurosr(Cursor c) {
		if (c != null && !c.isClosed()) {
			c.close();
		}
	}

}
